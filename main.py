import subprocess
import os
import sys
import threading

if getattr(sys, 'frozen', False):
    path = os.path.dirname(sys.executable)
else:
    path = os.path.dirname(os.path.abspath(__file__))

reference = "ref.fna"
bwa = os.path.join(path, "bwa.exe")
samtools = os.path.join(path, "samtools.exe")
extension = ".fastq.gz"

threads_count = 5
threads = 0


class calcThread(threading.Thread):
    def __init__(self, seq_type, name):
        threading.Thread.__init__(self)
        self.seq_type = seq_type
        self.name = name

    def run(self):
        global threads
        threads += 1
        sequence = self.seq_type + '_' + str(self.name)
        reads_1 = '_'.join([self.seq_type, str(self.name), '1'])
        reads_2 = '_'.join([self.seq_type, str(self.name), '2'])
        print('--aligning ' + sequence)
        subprocess.call(bwa + ' aln ' + reference + ' ' + reads_1 + extension + '  > ' + reads_1 + '.sai', cwd=path, shell=True, stdout=subprocess.DEVNULL)
        subprocess.call(bwa + ' aln ' + reference + ' ' + reads_2 + extension + '  > ' + reads_2 + '.sai', cwd=path, shell=True, stdout=subprocess.DEVNULL)
        print('--mapping ' + sequence)
        subprocess.call(bwa + ' sampe ' + reference + ' ' + reads_1 + '.sai ' + reads_2 + '.sai ' + reads_1 + extension + ' ' + reads_2 + extension + ' > ' + sequence + '.sam', cwd=path, shell=True, stdout=subprocess.DEVNULL)
        print('--convetring to bam ' + sequence)
        subprocess.call(samtools + ' view -S -b ' + sequence + '.sam > ' + sequence + '.bam', cwd=path, shell=True, stdout=subprocess.DEVNULL)
        print('--sorting bam ' + sequence)
        subprocess.call(samtools + ' sort ' + sequence + '.bam ' + sequence + '_sorted', cwd=path, shell=True, stdout=subprocess.DEVNULL)
        threads -= 1


if __name__ == "__main__":
    patterns = ['sensitive', 'resist']
    for seq_type in patterns:
        print('--Calculating ' + seq_type)
        for name in range(1, 7):
            while threads >= threads_count:
                pass
            calcThread(seq_type, name).start()
    while threads > 0:
        pass
    for seq_type in patterns:    
        print('--merging ' + seq_type)
        subprocess.call(samtools + ' merge ' + seq_type + '.bam ' + ' '.join([seq_type + '_' + str(i) + '_sorted.bam' for i in range(1, 7)]), cwd=path, shell=True, stdout=subprocess.DEVNULL)
